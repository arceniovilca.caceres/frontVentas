
export const LINK_PRINCIPAL = '/principal'
export const LINK_PRUEBA = '/prueba'
export const LINK_CONFIGURACION = '/configuracion'
export const VENTAS_URL = process.env.REACT_APP_SIS_VENTA
export const LOGIN_URL = process.env.REACT_APP_SIS_LOGIN

