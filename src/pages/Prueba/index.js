import Prueba from './Prueba';
import { LINK_PRUEBA } from '../../constants';

export default {
  route: {
    path: `${LINK_PRUEBA}`,
    component: Prueba,
  }
};
