import Principal from './Principal';
import { LINK_PRINCIPAL } from '../../constants';

export default {
  route: {
    path: `${LINK_PRINCIPAL}`,
    component: Principal,
  }
};
